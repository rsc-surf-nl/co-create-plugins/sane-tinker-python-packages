$LOGFILE = "c:\logs\SANE-tinker-python-packages.log"

Function Write-Log([String] $logText) {
  '{0:u}: {1}' -f (Get-Date), $logText | Out-File $LOGFILE -Append
}

Function Main {

    Write-Log "Start installing SANE-tinker-python-packages"

    $url =  [System.Environment]::GetEnvironmentVariable('REQ_URL')
    
    if ($url) {
      $FileName = $url.Split("/")[-1]
      
      $extn = [IO.Path]::GetExtension($FileName)
      if ($extn -eq ".txt" )
      {

        Remove-Item -Path C:\Test\hidden-RO-file.txt -Force

        $outpath = "$PSScriptRoot\url_requirements.txt"
        $wc = New-Object System.Net.WebClient  
        $wc.DownloadFile($url, $outpath)
        Write-Log "url file downloaded"
        foreach($line in Get-Content $PSScriptRoot\url_requirements.txt) {
          if(!($line.StartsWith('#'))){
              Write-Log "Package: $line "
              pip install $line 2>&1 >> $LOGFILE
          }
        }
      }else{
        Write-Log "File isn't a txt file"
      }
    }else{
      Write-Log "No url to requirements file given"
    }



    Write-Log "End installing SANE-tinker-python-packages"
}

Main  
